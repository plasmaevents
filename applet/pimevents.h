/*
 *   Copyright (C) 2007 Filip Brcic <brcha@gna.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License version 2 as
 *   published by the Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __PLASMA_EVENTS__H__
#define __PLASMA_EVENTS__H__

#include <plasma/applet.h>
#include <plasma/widgets/label.h>
#include <plasma/layouts/boxlayout.h>

// KdePIM headers should come here
 
class PlasmaEvents : public Plasma::Applet
{
    Q_OBJECT
  public:
    PlasmaEvents(QObject *parent, const QVariantList &args);
    ~PlasmaEvents();

  private slots:
//     void dataUpdated(const QString& sourceName, const Plasma::DataEngine::Data& data);
    void constraintsUpdated(Plasma::Constraints constraints);
 
  private:
    Plasma::Label * m_display;
    int m_updateInterval;
    Plasma::BoxLayout * m_layout;
};
 
// This is the command that links your applet to the .desktop file
K_EXPORT_PLASMA_APPLET(events, PlasmaEvents)
#endif /* __PLASMA_EVENTS__H__ */
