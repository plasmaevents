/*
 *   Copyright (C) 2007 Filip Brcic <brcha@gna.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License version 2 as
 *   published by the Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "plasmaevents.h"

#include <kdebug.h>
 
PlasmaEvents::PlasmaEvents(QObject *parent, const QVariantList &args)
    : Plasma::Applet(parent, args)
{
  m_layout = new Plasma::BoxLayout(Plasma::BoxLayout::TopToBottom);

  // m_engine = myPimEngine(...)
  // m_engine->connectSource(...)
  
  setSize(125, 125);

  m_display = new Plasma::Label(this);
  m_display->setPen(QPen(QColor(Qt::white)));
  m_display->setAlignment(Qt::AlignLeft);
  m_display->setText(i18n("What ever should be written here"));

  m_layout->addItem(m_display);
}
 
PlasmaEvents::~PlasmaEvents()
{
  delete m_display;
  delete m_layout;
}

void PlasmaEvents::constraintsUpdated(Plasma::Constraints constraints)
{
  setSize(size());
  
  if (constraints & Plasma::FormFactorConstraint)
  {
    updateGeometry();
  }
}

// void PlasmaEvents::dataUpdated(const QString& sourceName, const Plasma::DataEngine::Data& data)
// {
//   Q_UNUSED(data);
//   Q_UNUSED(sourceName);
// }


#include "plasmaevents.moc"
